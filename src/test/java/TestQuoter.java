import nl.utwente.di.bookQuote.Quoter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestQuoter {

    @Test
    public void testBook1() throws Exception {
        Quoter quoter = new Quoter();
        double price1 = quoter.getBookPrice("1");
        double price2 = quoter.getBookPrice("2");
        Assertions.assertEquals(10.0, price1, 0.0, "Price of book 1");
        Assertions.assertEquals(45.0, price2, 0.0, "Price of book 2");
    }
}
