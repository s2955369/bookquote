package nl.utwente.di.bookQuote;


import java.util.HashMap;

public class Quoter {

    private HashMap<String, Double> quotes;

    public void load() {
        quotes = new HashMap<>();
        quotes.put("1", 10.0);
        quotes.put("2", 45.0);
        quotes.put("3", 20.0);
        quotes.put("4", 35.0);
        quotes.put("5", 50.0);
    }
    public double getBookPrice(String isbn) {
        load();
        if (quotes.containsKey(isbn)) {
            return quotes.get(isbn);
        }
        return 0;
    }
}
